//
//  ListItemCell.swift
//  FavPlaces
//
//  Created by Martyna Wiśnik on 27.12.2017.
//  Copyright © 2017 Martyna Wiśnik. All rights reserved.
//

import UIKit

class ListItemCell: UITableViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeTitleLabel: UILabel!
    @IBOutlet weak var placeDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
