//
//  PlaceModel.swift
//  FavPlaces
//
//  Created by Martyna Wiśnik on 27.12.2017.
//  Copyright © 2017 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class PlaceModel {
    
    var image: UIImage?
    var title: String?
    var description: String?
    
    init(image: UIImage? = nil, title: String? = nil, description: String? = nil) {
        self.image = image
        self.title = title
        self.description = description
    }
    
}
