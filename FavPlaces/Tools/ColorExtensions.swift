//
//  ColorExtensions.swift
//  FavPlaces
//
//  Created by Martyna Wiśnik on 31.12.2017.
//  Copyright © 2017 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class var textViewBackgroundColor: UIColor {
        return UIColor(red: 227.0/255.0, green: 231.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    }
    
    class var navigationBarColor: UIColor {
        return UIColor(red: 242.0/255.0, green: 116.0/255.0, blue: 119.0/255.0, alpha: 1.0)
    }
}
