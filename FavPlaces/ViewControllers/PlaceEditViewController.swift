//
//  PlaceEditViewController.swift
//  FavPlaces
//
//  Created by Martyna Wiśnik on 28.12.2017.
//  Copyright © 2017 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos

protocol SavePlaceDelegate {
    func didSavePlace(place: PlaceModel)
    func didEditPlace(place: PlaceModel)
}

class PlaceEditViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate: SavePlaceDelegate?
    var place: PlaceModel?
    var editMode: Bool!
    var photoAdded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descTextView.delegate = self
        descTextView.layer.cornerRadius = 10.0
        descTextView.backgroundColor = UIColor.textViewBackgroundColor
        titleTextField.backgroundColor = UIColor.textViewBackgroundColor
        title = "Place details"
        scrollView.alwaysBounceVertical = true
        saveButton.layer.cornerRadius = 26.0
        if let place = place {
            self.placeImageView.image = place.image
            self.titleTextField.text = place.title
            self.descTextView.text = place.description
        } else {
            placeImageView.image = UIImage(named: "image")
        }
    }
    
    @IBAction func imageViewTapped(_ sender: Any) {
        showMediaActionSheet()
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        savePlace()
    }
    
    
    func showMediaActionSheet() {
        let alertController = UIAlertController(title: "Choose media source", message: "Choose where the image should come from:", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.checkCameraPermissions()
        })
        
        let cameraRollAction = UIAlertAction(title: "Photo library", style: .default, handler: { (action) -> Void in
            self.checkCameraRollPermissions()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
        alertController.addAction(cameraAction)
        alertController.addAction(cameraRollAction)
        alertController.addAction(cancelAction)
        
        self.navigationController?.present(alertController, animated: true, completion: nil)
    }
    
    func savePlace() {
        if let image =  self.placeImageView.image, let title = titleTextField.text, let desc = descTextView.text {
            if title != "" && desc != "" {
                if editMode {
                    self.delegate?.didEditPlace(place: PlaceModel(image: image, title: title, description: desc))
                } else if self.photoAdded {
                    self.delegate?.didSavePlace(place: PlaceModel(image: image, title: title, description: desc))
                } else {
                    self.showEmptyAlert()
                }
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showEmptyAlert()
            }
        } else {
            
            
            self.showEmptyAlert()
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        let pointInTable:CGPoint = textView.superview!.convert(textView.frame.origin, to: scrollView)
        var contentOffset:CGPoint = scrollView.contentOffset
        contentOffset.y  = pointInTable.y/1.5
        if let accessoryView = textView.inputAccessoryView {
            contentOffset.y -= accessoryView.frame.size.height
        }
        scrollView.contentOffset = contentOffset
        return true
    }
    
    func showEmptyAlert() {
        let alert = UIAlertController(title: "Incomplete data", message: "You must provide all required information (image, title, description) to save a place.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension PlaceEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func checkCameraPermissions() {
        let deviceHasCamera = UIImagePickerController.isSourceTypeAvailable(.camera)
        if deviceHasCamera {
            let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            switch authStatus {
            case .authorized: openCamera()
            case .denied: showDeniedAlert()
            case .notDetermined: requestCameraPermissions()
            default: showDeniedAlert()
                
            }
        }
    }
    
    func checkCameraRollPermissions() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            PHPhotoLibrary.requestAuthorization({ status in
                switch status {
                case .authorized: self.openPhotoLibrary()
                case .denied: self.showDeniedAlert()
                default: self.showDeniedAlert()
                }
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[ .originalImage] as? UIImage {
            placeImageView.image = image
            self.photoAdded = true
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func requestCameraPermissions() {
        if AVCaptureDevice.devices(for: AVMediaType.video).count > 0 {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                if granted {
                    self.openCamera()
                }
            })
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func showDeniedAlert() {
        let alert = UIAlertController(title: "Access denied", message: "Permissions to camera not granted. You can change it in settings.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Open settings", style: .default) { alert in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        })
        self.present(alert, animated: true, completion: nil)
    }
}
