//
//  ListViewController.swift
//  FavPlaces
//
//  Created by Martyna Wiśnik on 27.12.2017.
//  Copyright © 2017 Martyna Wiśnik. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SavePlaceDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    var placesList = PlacesListModel()
    var placeData: PlaceModel?
    var editMode: Bool?
    var id: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        let leftButton = UIBarButtonItem(title: "Edit", style: UIBarButtonItem.Style.plain, target: self, action: #selector(showEditing))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "My favourite places"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showHideEmptyState()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesList.places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell") as! ListItemCell
        cell.placeImageView.image = placesList.places[indexPath.row].image
        cell.placeTitleLabel.text = placesList.places[indexPath.row].title
        cell.placeDescriptionLabel.text = placesList.places[indexPath.row].description
        cell.placeImageView.layer.cornerRadius = 40.0

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.placeData = placesList.places[indexPath.row]
        self.editMode = true
        self.id = indexPath.row
        self.tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to toIndexPath: IndexPath) {
        let itemToMove = placesList.places[fromIndexPath.row]
        placesList.places.remove(at: fromIndexPath.row)
        placesList.places.insert(itemToMove, at: toIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            placesList.places.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath as IndexPath], with: UITableView.RowAnimation.automatic)
            showHideEmptyState()
        }
    }
    
    func didSavePlace(place: PlaceModel) {
        placesList.places.append(place)
        tableView.reloadData()
    }
    
    func didEditPlace(place: PlaceModel) {
        if let id = self.id {
            placesList.places[id] = place
        }
        tableView.reloadData()
    }
    
    func showHideEmptyState() {
        if placesList.places.count == 0 {
            self.emptyView.isHidden = false
        } else {
            self.emptyView.isHidden = true
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        title = ""
        self.tableView.isEditing = false
        self.navigationItem.leftBarButtonItem?.title = "Edit"
        if segue.identifier == "showDetails" {
            let destinationVC = segue.destination as! PlaceEditViewController
            destinationVC.delegate = self
            destinationVC.place = self.placeData
            destinationVC.editMode = self.editMode
            self.placeData = nil
        }
    }
    @IBAction func plusButtonTapped(_ sender: Any) {
        self.editMode = false
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    @objc func showEditing(sender: UIBarButtonItem) {
        self.tableView.isEditing = !self.tableView.isEditing
        if self.tableView.isEditing {
            self.navigationItem.leftBarButtonItem?.title = "Done"
        } else {
            self.navigationItem.leftBarButtonItem?.title = "Edit"
        }
    }
    
}

